<?php
/*
Template Name: דף הבית
*/

get_header();
$fields = get_fields();
if ($fields['home_video_file']) : ?>
	<div class="video-home">
		<video id="video-home">
			<source src="<?= $fields['home_video_file']['url']; ?>" type="video/mp4">
		</video>
		<span id="video-button" class="button-home" <?php if ($fields['home_video_img']) : ?>
			  style="background-image: url('<?= $fields['home_video_img']['url']; ?>')" <?php endif; ?>>
			<span class="overlay-home">
				<img src="<?= ICONS ?>play.png" alt="play-video">
			</span>
		</span>
	</div>
<?php endif;
get_template_part('views/partials/content', 'slider_departments', [
		'content' => $fields['home_departments'],
]);
if ($fields['h_about_text']) : ?>
	<section class="about-block">
		<div class="container">
			<div class="row justify-content-between align-items-center">
				<?php if ($fields['h_about_img']) : ?>
					<div class="col-xl-5 col-lg-6 col-12">
						<div class="photo-wrapper">
							<img src="<?= $fields['h_about_img']['url']; ?>" alt="<?= $fields['h_about_img']['alt']; ?>">
						</div>
					</div>
				<?php endif; ?>
				<div class="<?= $fields['h_about_img'] ? 'col-lg-6 col-12' : 'col-12'; ?>">
					<div class="base-output">
						<?= $fields['h_about_text']; ?>
					</div>
					<?php if ($fields['h_about_link']) : ?>
						<div class="row justify-content-end">
							<div class="col-auto">
								<a href="<?= $fields['h_about_link']['url'];?>" class="read-more-link">
									<?= (isset($fields['h_about_link']['title']) && $fields['h_about_link']['title'])
											? $fields['h_about_link']['title'] : esc_html__('קראו עוד', 'leos');
									?>
								</a>
							</div>
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</section>
<?php endif;
if ($fields['h_content_text']) : ?>
	<section class="home-content-block">
		<div class="container">
			<div class="row justify-content-between align-items-center">
				<div class="<?= $fields['h_content_img'] ? 'col-lg-6 col-12' : 'col-12'; ?>">
					<div class="base-output">
						<?= $fields['h_content_text']; ?>
					</div>
					<?php if ($fields['h_content_link']) : ?>
						<div class="row justify-content-start">
							<div class="col-auto">
								<a href="<?= $fields['h_content_link']['url'];?>" class="read-more-link">
									<?= (isset($fields['h_content_link']['title']) && $fields['h_content_link']['title'])
											? $fields['h_content_link']['title'] : esc_html__('קראו עוד', 'leos');
									?>
								</a>
							</div>
						</div>
					<?php endif; ?>
				</div>
				<?php if ($fields['h_content_img']) : ?>
					<div class="col-xl-5 col-lg-6 col-12 h-content-img-col">
						<div class="photo-wrapper">
							<div class="photo-insider">
								<?php if ($logo = opt('logo')) : ?>
									<div class="logo">
										<img src="<?= $logo['url']; ?>" alt="logo">
									</div>
								<?php endif; ?>
								<img src="<?= $fields['h_content_img']['url']; ?>" alt="<?= $fields['h_content_img']['alt']; ?>">
							</div>
						</div>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</section>
<?php endif; ?>
<section class="form-homepage-section">
	<div class="col-half-form" <?php if ($fields['h_form_back']) : ?>
		style="background-image: url('<?= $fields['h_form_back']; ?>')"
	<?php endif; ?>></div>
	<div class="col-half-facebook"></div>
	<div class="container">
		<div class="row justify-content-between align-items-center">
			<div class="col-xl-5 col-lg-6 pb-lg-0 pb-5">
				<?php if ($fields['h_form_title']) : ?>
					<h2 class="base-form-title"><?= $fields['h_form_title']; ?></h2>
				<?php endif;
				if ($fields['h_form_subtitle']) : ?>
					<h3 class="base-form-subtitle"><?= $fields['h_form_subtitle']; ?></h3>
				<?php endif;
				getForm('8'); ?>
			</div>
			<div class="col-lg-6 pt-lg-0 pt-5">
				<div class="row">
					<div class="col-sm-8 col-12 facebook-col">
						<?php if ($fields['h_facebook_title']) : ?>
							<h2 class="base-form-title"><?= $fields['h_facebook_title']; ?></h2>
						<?php endif;
						if ($fields['h_facebook_subtitle']) : ?>
							<h3 class="base-form-subtitle"><?= $fields['h_facebook_subtitle']; ?></h3>
						<?php endif;
						if ($facebook = opt('facebook')) : ?>
							<a href="<?= $facebook; ?>" class="link-to-facebook mt-4">
								<?= esc_html__('מעבר לדף הפייסבוק', 'leos'); ?>
							</a>
						<?php endif; ?>
					</div>
					<?php if ($fields['h_facebook_img']) : ?>
						<div class="col-4 face-img-col">
							<img src="<?= $fields['h_facebook_img']['url']; ?>" alt="<?= $fields['h_facebook_img']['alt']; ?>">
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</section>
<?php if ($fields['h_video_slider']) : ?>
	<section class="gallery-slider-home arrows-slider">
		<div class="container">
			<div class="row">
				<div class="col-12 mb-5">
					<?php if ($fields['h_video_title']) : ?>
						<h2 class="block-title">
							<?= $fields['h_video_title']; ?>
						</h2>
					<?php endif;
					if ($fields['h_video_subtitle']) : ?>
						<h3 class="block-subtitle mb-lg-5 mb-2">
							<?= $fields['h_video_subtitle']; ?>
						</h3>
					<?php endif; ?>
				</div>
			</div>
			<div class="row justify-content-center">
				<div class="col-xl-9 col-sm-12 col-11">
					<div class="base-slider" dir="rtl">
						<?php foreach ($fields['h_video_slider'] as $video_slide) : ?>
							<div>
								<div class="row justify-content-center align-items-center">
									<?php if ($video_slide['slider_text']) : ?>
										<div class="<?= $video_slide['video_link'] ? 'col-lg-6 col-12' : 'col-12'; ?>">
											<div class="base-output px-3">
												<?= $video_slide['slider_text']; ?>
											</div>
										</div>
									<?php endif;
									if ($video_slide['video_link']) : ?>
										<div class="<?= $video_slide['slider_text'] ? 'col-lg-6 col-12' : 'col-12'; ?>">
											<div class="put-video-wrapper">
												<div class="put-video-here" style="background-image: url('<?= getYoutubeThumb($video_slide['video_link']); ?>')">
													<div class="put-video-inside"></div>
													<span class="play-video-here" data-id="<?= getYoutubeId($video_slide['video_link']); ?>">
														<img src="<?= ICONS ?>play.png" alt="play-video">
													</span>
												</div>
											</div>
										</div>
									<?php endif; ?>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php endif;
if ($fields['h_posts_1']) {
	get_template_part('views/partials/content', 'articles', [
			'title' => $fields['h_posts_title_1'],
			'subtitle' => $fields['h_posts_subtitle_1'],
			'link' => $fields['h_posts_link_1'],
			'posts' => $fields['h_posts_1'],
			'link_title' => esc_html__('ללוח האירועים', 'leos'),
	]);
}
if ($fields['h_posts_2']) {
	get_template_part('views/partials/content', 'articles', [
		'title' => $fields['h_posts_title_2'],
		'subtitle' => $fields['h_posts_subtitle_2'],
		'link' => $fields['h_posts_link_2'],
		'posts' => $fields['h_posts_2'],
		'link_title' => esc_html__('לכל המאמרים', 'leos'),
	]);
}
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
			'content' => $fields['single_slider_seo'],
			'img' => $fields['slider_img'],
	]);
}
if ($fields['h_gallery_images'] || $fields['h_gallery_videos']) : ?>
	<section class="gallery-home arrows-slider">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-12">
					<h2 class="block-title mb-4">
						<?= $fields['h_gallery_title'] ? $fields['h_gallery_title'] : esc_html__('הגלריה שלנו', 'leos');; ?>
					</h2>
				</div>
			</div>
			<?php if ($fields['h_gallery_images']) : ?>
				<div class="row">
					<div class="col-12">
						<div class="home-gallery-slider" dir="rtl">
							<?php foreach ($fields['h_gallery_images'] as $x => $post) : ?>
								<div class="gal-slide-item">
									<?php get_template_part('views/partials/card', 'gallery_image',
											[
													'item' => $post,
											]);
									?>
								</div>
							<?php endforeach; ?>
						</div>
					</div>
				</div>
			<?php endif;
			if ($fields['h_gallery_videos']) : ?>
				<div class="row">
					<div class="col-12">
						<div class="home-gallery-slider" dir="rtl">
							<?php foreach ($fields['h_gallery_videos'] as $x => $post) : ?>
								<div class="gal-slide-item">
									<?php get_template_part('views/partials/card', 'gallery_video_home',
											[
													'item' => $post,
											]);
									?>
								</div>
							<?php endforeach; ?>
						</div>
					</div>
				</div>
				<div class="video-modal">
					<div class="modal fade" id="modalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
						 aria-hidden="true">
						<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
							<div class="modal-content">
								<div class="modal-body" id="iframe-wrapper"></div>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true" class="close-icon">×</span>
								</button>
							</div>
						</div>
					</div>
				</div>
			<?php endif;
			if ($fields['h_gallery_link']) : ?>
				<div class="row justify-content-center mt-5">
					<div class="col-auto">
						<a href="<?= $fields['h_gallery_link']['url'];?>" class="base-link">
							<?= (isset($fields['h_gallery_link']['title']) && $fields['h_gallery_link']['title'])
									? $fields['h_gallery_link']['title'] : esc_html__('עברו לגלריה', 'leos');
							?>
						</a>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</section>
<?php endif;
if ($fields['faq_item']) :
	get_template_part('views/partials/content', 'faq',
			[
					'text' => $fields['faq_text'],
					'faq' => $fields['faq_item'],
			]);
endif;
get_footer(); ?>
