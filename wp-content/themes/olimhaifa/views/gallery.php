<?php
/*
Template Name: גלריה
*/

get_header();
$fields = get_fields();
$count_posts = '';
if ($fields['page_gallery_images']) {
	$count_posts = count($fields['page_gallery_images']);
}
$count_posts_videos = '';
if ($fields['page_gallery_videos']) {
	$count_posts_videos = count($fields['page_gallery_videos']);
}
$page_id = get_queried_object_id();
?>
<article class="page-body gallery-body">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-12">
				<h1 class="block-title">
					<?php the_title(); ?>
				</h1>
				<h2 class="block-subtitle">
					<?= $fields['about_subtitle']; ?>
				</h2>
				<div class="base-output text-center">
					<?php the_content(); ?>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row align-items-stretch justify-content-center">
			<div class="col-auto">
				<ul class="nav nav-tabs" id="type-gallery-tab" role="tablist">
					<?php if ($fields['page_gallery_images']) : ?>
						<li class="nav-item tab-style">
							<a class="nav-link active" id="images-tab" data-toggle="tab" href="#images-tab-1"
							   role="tab" aria-controls="type" aria-selected="true">
								<?= esc_html__('תמונות', 'leos'); ?>
							</a>
						</li>
					<?php endif;
					if ($fields['page_gallery_videos']) : ?>
						<li class="nav-item tab-style">
							<a class="nav-link" id="videos-tab" data-toggle="tab" href="#videos-tab-1"
							   role="tab" aria-controls="type" aria-selected="false">
								<?= esc_html__('וידאו', 'leos'); ?>
							</a>
						</li>
					<?php endif; ?>
				</ul>
			</div>
		</div>
		<div class="row">
			<div class="col-12 tab-content">
				<?php if ($fields['page_gallery_images']) : ?>
					<div class="tab-pane fade show active" id="images-tab-1" role="tabpanel" aria-labelledby="images-tab">
						<div class="row put-here-posts align-items-stretch justify-content-center">
							<?php foreach ($fields['page_gallery_images'] as $x => $post) {
								if ($x < 15) {
									get_template_part('views/partials/card', 'gallery_image',
											[
													'item' => $post,
													'num' => $x,
											]);
								}
							} ?>
						</div>
						<?php if ($count_posts && $count_posts > 15) : ?>
							<div class="row justify-content-center">
								<div class="col-auto">
									<div class="more-link base-link load-more-posts" data-type="images" data-page="<?= $page_id; ?>">
										<?= esc_html__('לחצו לעוד תמונות', 'leos'); ?>
									</div>
								</div>
							</div>
						<?php endif; ?>
					</div>
				<?php endif;
				if ($fields['page_gallery_videos']) : ?>
					<div class="tab-pane fade <?= !$fields['page_gallery_images'] ? 'show active' : '';?>" id="videos-tab-1" role="tabpanel" aria-labelledby="videos-tab">
						<div class="row put-here-posts align-items-stretch justify-content-center">
							<?php foreach ($fields['page_gallery_videos'] as $x => $post) {
								if ($x < 15) {
									get_template_part('views/partials/card', 'gallery_video',
											[
													'item' => $post,
													'num' => $x,
											]);
								}
							} ?>
						</div>
						<?php if ($count_posts && $count_posts > 15) : ?>
							<div class="row justify-content-center">
								<div class="col-auto">
									<div class="more-link base-link load-more-posts" data-type="videos" data-page="<?= $page_id; ?>">
										<?= esc_html__('לחצו לעוד וידאו', 'leos'); ?>
									</div>
								</div>
							</div>
						<?php endif; ?>
					</div>
					<div class="video-modal">
						<div class="modal fade" id="modalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
							 aria-hidden="true">
							<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
								<div class="modal-content">
									<div class="modal-body" id="iframe-wrapper"></div>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true" class="close-icon">×</span>
									</button>
								</div>
							</div>
						</div>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</article>
<?php get_template_part('views/partials/repeat', 'form');
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
			'content' => $fields['single_slider_seo'],
			'img' => $fields['slider_img'],
	]);
}
get_footer(); ?>

