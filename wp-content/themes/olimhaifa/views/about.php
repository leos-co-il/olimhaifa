<?php
/*
Template Name: אודות
*/

get_header();
$fields = get_fields();
?>

<article class="about-page-body">
	<section class="top-about" <?php if (has_post_thumbnail()) : ?>
		style="background-image: url('<?= postThumb(); ?>')"
	<?php endif; ?>>
		<div class="top-about-overlay"></div>
	</section>
	<?php get_template_part('views/partials/content', 'slider_departments', [
			'content' => $fields['about_departments'],
	]); ?>
	<div class="container mt-5">
		<div class="row justify-content-center">
			<div class="col-xxl-10 col-12">
				<div class="row justify-content-xl-between justify-content-center align-items-center">
					<div class="<?= $fields['gallery'] ? 'col-xl-6 col-12' : 'col-12'; ?>">
						<h1 class="block-title">
							<?php the_title(); ?>
						</h1>
						<h2 class="block-subtitle">
							<?= $fields['about_subtitle']; ?>
						</h2>
						<div class="base-output">
							<?php the_content(); ?>
						</div>
					</div>
					<?php if ($fields['gallery']) : ?>
						<div class="col-xl-5 col-md-10 col-sm-11 col-12 my-xl-0 my-5">
							<div class="about-slider-wrap arrows-slider">
								<div class="photo-wrapper">
									<div class="photo-insider">
										<?php if ($logo = opt('logo')) : ?>
											<div class="logo">
												<img src="<?= $logo['url']; ?>" alt="logo">
											</div>
										<?php endif; ?>
										<div class="base-slider" dir="rtl">
											<?php foreach ($fields['gallery'] as $image) : ?>
												<div>
													<a href="<?= $image['url']; ?>" data-lightbox="about-haifa-gallery"
													   style="background-image: url('<?= $image['url']; ?>')" class="about-slide"></a>
												</div>
											<?php endforeach; ?>
										</div>
									</div>
								</div>
							</div>
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</article>
<?php if ($fields['about_post_slider']) :
	$link = ($fields['about_post_link']) ? $fields['about_post_link'] : '';
	$link_title = (isset($args['link_title']) && $args['link_title']) ? $args['link_title'] : 'לכל המאמרים';
	?>
	<section class="home-posts-block arrows-slider pt-5 post-about-slider">
		<div class="container">
			<div class="row">
				<div class="col-12 mb-3">
					<?php if ($fields['about_post_title']) : ?>
						<h2 class="block-title text-center">
							<?= $fields['about_post_title']; ?>
						</h2>
					<?php endif; ?>
				</div>
			</div>
			<div class="row justify-content-center align-items-stretch">
				<div class="col-12">
					<div class="post-slider" dir="rtl">
						<?php foreach ($fields['about_post_slider'] as $post) : ?>
							<div>
								<?php get_template_part('views/partials/card', 'post', [
										'post' => $post,
								]); ?>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
			<?php if ($link) : ?>
				<div class="row justify-content-center mt-4">
					<div class="col-auto">
						<a href="<?= $link['url'];?>" class="base-link">
							<?= (isset($link['title']) && $link['title']) ? $link['title'] : $link_title; ?>
						</a>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</section>
<?php endif; ?>
<?php
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
			'content' => $fields['single_slider_seo'],
			'img' => $fields['slider_img'],
	]);
}
get_footer(); ?>
