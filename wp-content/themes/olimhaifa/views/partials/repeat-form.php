<section class="repeat-form-block mb-4">
	<div class="container">
		<div class="row justify-content-center form-wrapper">
			<div class="col-12">
				<?php if ($title = opt('base_form_title')) : ?>
					<h2 class="base-form-title text-center"><?= $title; ?></h2>
				<?php endif;
				if ($subtitle = opt('base_form_subtitle')) : ?>
					<h3 class="base-form-subtitle text-center"><?= $subtitle; ?></h3>
				<?php endif;
				getForm('9'); ?>
			</div>
		</div>
	</div>
</section>
