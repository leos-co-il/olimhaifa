<?php if(isset($args['item']) && $args['item']) :  ?>
	<div class="col-lg-4 col-sm-6 col-12 gallery-col">
		<div class="gallery-item more-card" data-id="<?= isset($args['num']) && $args['num'] ? $args['num'] : '' ?>">
			<a class="gallery-img" style="background-image: url('<?= $args['item']['url']; ?>')" href="<?= $args['item']['url']; ?>" data-lightbox="gallery">
			</a>
		</div>
	</div>
<?php endif; ?>
