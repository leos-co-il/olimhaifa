<?php if (isset($args['faq']) && $args['faq']) : ?>
	<div class="faq">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-auto">
					<?php if (isset($args['text']) && $args['text']) : ?>
						<div class="base-output">
							<?= $args['text']; ?>
						</div>
					<?php else: ?>
						<h2 class="block-title"><?= esc_html__('שאלות ותשובות נפוצות', 'leos'); ?></h2>
						<h2 class="block-subtitle"><?= esc_html__('אתם שואלים - אנחנו עונים', 'leos'); ?></h2>
					<?php endif; ?>
				</div>
			</div>
			<div class="row justify-content-center">
				<div class="col-xl-6 col-lg-9 col-md-11 col-12">
					<div id="accordion">
						<?php foreach ($args['faq'] as $num => $item) : ?>
							<div class="card question-card wow fadeInUp" data-wow-delay="0.<?= $num + $i = 1; ?>s" <?php $i++; ?>>
								<div class="question-header" id="heading_<?= $num; ?>">
									<button class="question-title" data-toggle="collapse"
											data-target="#faqChild<?= $num; ?>"
											aria-expanded="false" aria-controls="collapseOne">
										<span class="faq-icon">
											?
										</span>
										<span class="faq-body-title"><?= $item['faq_question']; ?></span>
									</button>
									<div id="faqChild<?= $num; ?>" class="collapse faq-item answer-body"
										 aria-labelledby="heading_<?= $num; ?>" data-parent="#accordion">
										<div class="base-output small-output">
											<?= $item['faq_answer']; ?>
										</div>
									</div>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>
