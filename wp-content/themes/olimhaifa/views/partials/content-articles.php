<?php if (isset($args['posts']) && $args['posts']) :
	$title = (isset($args['title']) && $args['title']) ? $args['title'] : '';
	$subtitle = (isset($args['subtitle']) && $args['subtitle']) ? $args['subtitle'] : '';
	$link = (isset($args['link']) && $args['link']) ? $args['link'] : '';
	$link_title = (isset($args['link_title']) && $args['link_title']) ? $args['link_title'] : '';
	?>
	<section class="home-posts-block">
		<div class="container">
			<div class="row">
				<div class="col-12 mb-5">
					<?php if ($title) : ?>
						<h2 class="block-title">
							<?= $title; ?>
						</h2>
					<?php endif;
					if ($subtitle) : ?>
						<h3 class="block-subtitle">
							<?= $subtitle; ?>
						</h3>
					<?php endif; ?>
				</div>
			</div>
			<div class="row justify-content-center align-items-stretch">
				<?php foreach ($args['posts'] as $post) {
					get_template_part('views/partials/card', 'post',
						[
							'post' => $post,
						]);
				} ?>
			</div>
			<?php if ($link) : ?>
				<div class="row justify-content-end mt-4">
					<div class="col-auto">
						<a href="<?= $link['url'];?>" class="base-link">
							<?= (isset($link['title']) && $link['title']) ? $link['title'] : $link_title; ?>
						</a>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</section>
<?php endif; ?>
