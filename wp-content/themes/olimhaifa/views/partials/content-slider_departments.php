<?php $slider = (isset($args['content']) && $args['content']) ? $args['content'] : get_terms([
	'taxonomy' => 'department_cat',
	'hide_empty' => false,
]); if ($slider) : ?>
	<div class="departments-slider arrows-slider slider-departments-arrows">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="slider-deps" dir="rtl">
						<?php foreach ($slider as $slide) : ?>
							<div class="dep-slide-wrap">
								<a class="department-slide-item" href="<?= get_term_link($slide); ?>">
									<?php if ($icon = get_field('cat_icon', $slide)) : ?>
										<span class="cat-slide-icon">
											<img src="<?= $icon['url']; ?>" alt="<?= $icon['alt']; ?>">
										</span>
									<?php endif; ?>
									<span class="slide-dep-white">
										<span class="department-title">
											<?= $slide->name; ?>
										</span>
									</span>
								</a>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>

