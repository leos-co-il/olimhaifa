<?php if(isset($args['item']) && isset($args['item']['video_link']) && $args['item']['video_link']) :  ?>
	<div class="col-lg-4 col-sm-6 col-12 gallery-col">
		<div class="gallery-item">
			<div class="gallery-img" style="background-image: url('<?= getYoutubeThumb($args['item']['video_link']); ?>')">
				<div class="gallery-overlay play-button" data-video="<?= getYoutubeId($args['item']['video_link']); ?>">
					<img src="<?= ICONS ?>play.png" alt="play-video">
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>
