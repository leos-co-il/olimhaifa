<?php if(isset($args['item']) && isset($args['item']['video_url']) && $args['item']['video_url']) :  ?>
	<div class="col-lg-4 col-sm-6 col-12 gallery-col">
		<div class="gallery-item more-card" data-id="<?= isset($args['num']) && $args['num'] ? $args['num'] : '' ?>">
			<div class="gallery-img" style="background-image: url('<?= getYoutubeThumb($args['item']['video_url']); ?>')">
				<div class="gallery-overlay play-button" data-video="<?= getYoutubeId($args['item']['video_url']); ?>">
					<img src="<?= ICONS ?>play.png" alt="play-video">
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>
