<?php
/*
Template Name: צור קשר
*/

get_header();
$fields = get_fields();
?>
<article class="contact-page">
	<div class="contact-container">
		<div class="container position-relative">
			<div class="row justify-content-center">
				<div class="col-xl-8 col-sm-11 col-12">
					<h1 class="block-title">
						<?php the_title(); ?>
					</h1>
					<h2 class="block-subtitle mb-4">
						<?= $fields['about_subtitle']; ?>
					</h2>
					<div class="base-output text-center">
						<?php the_content(); ?>
					</div>
				</div>
				<div class="col-xl-8 col-sm-11 col-12">
					<img src="<?= IMG ?>phone-contact.png" alt="phone" class="contact-phone">
					<div class="form-contact-block" <?php if (has_post_thumbnail()) : ?>
						style="background-image: url('<?= postThumb(); ?>')"
					<?php endif; ?>>
						<?php if ($fields['contact_form_title']) : ?>
							<h3 class="contact-form-title">
								<?= $fields['contact_form_title']; ?>
							</h3>
						<?php endif;
						getForm('10'); ?>
						<div class="page-contacts-block">
							<ul class="row contact-list justify-content-start align-items-start">
								<?php if ($address = opt('address')) : ?>
									<li class="contact-item col-lg col-12">
										<a href="https://waze.com/ul?q=<?= $address; ?>"
										   class="contact-info-item" target="_blank">
											<span class="contact-icon-wrap">
												<img src="<?= ICONS ?>contact-geo.png">
											</span>
											<span class="contact-item-title">
												<?= esc_html__('כתובתנו:', 'leos'); ?>
											</span>
											<span class="contact-item-text"><?= $address; ?></span>
										</a>
									</li>
								<?php endif; ?>
								<?php if ($tel = opt('tel')) : ?>
									<li class="contact-item col-lg col-12">
										<a href="tel:<?= $tel; ?>" class="contact-info-item">
											<span class="contact-icon-wrap">
												<img src="<?= ICONS ?>contact-tel.png">
											</span>
											<span class="contact-item-title">
												<?= esc_html__('טלפון:', 'leos'); ?>
											</span>
											<span class="contact-item-text"><?= $tel; ?></span>
										</a>
									</li>
								<?php endif; ?>
								<?php if ($mail = opt('mail')) : ?>
									<li class="contact-item col-lg col-12">
										<a href="mailto:<?= $mail; ?>" class="contact-info-item">
											<span class="contact-icon-wrap">
												<img src="<?= ICONS ?>contact-mail.png">
											</span>
											<span class="contact-item-title">
												<?= esc_html__('מייל:', 'leos'); ?>
											</span>
											<span class="contact-item-text"><?= $mail; ?></span>
										</a>
									</li>
								<?php endif; ?>
							</ul>
						</div>
					</div>
				</div>
				<?php if ($fields['main_phones']) : ?>
					<div class="col-xl-8 col-sm-11 col-12">
						<?php if ($fields['main_phones_title']) : ?>
							<h3 class="block-subtitle main-phones-title">
								<?= $fields['main_phones_title']; ?>
							</h3>
						<?php endif; ?>
						<div class="row justify-content-start align-items-start">
							<?php foreach ($fields['main_phones'] as $item) : ?>
								<div class="col-xl-4 col-md-6 col-12 main-contact-col">
									<div class="main-phone-item">
										<span class="menu-icon-contact">
											<img src="<?= ICONS ?>menu-icon.png" alt="icon">
										</span>
										<div>
											<?php if ($item['title']) : ?>
												<h4 class="main-phone-title">
													<?= $item['title']; ?>
												</h4>
											<?php endif;
											if ($item['name'] || $item['phone']) : ?>
												<span class="main-phone-text">
													<?= $item['name']; ?>
												</span>
												<a href="tel:<?= $item['phone']; ?>" class="main-phone-text">
													<?= $item['phone']; ?>
												</a>
											<?php endif; ?>
										</div>
									</div>
								</div>
							<?php endforeach; ?>
						</div>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</article>
<?php get_footer(); ?>
