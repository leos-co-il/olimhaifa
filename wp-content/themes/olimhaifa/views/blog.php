<?php
/*
Template Name: מאמרים
*/

get_header();
$fields = get_fields();
$post_type = $fields['type'] ? $fields['type'] : 'post';
$posts = new WP_Query([
		'posts_per_page' => 8,
		'post_type' => $post_type,
		'suppress_filters' => false
]);
$published_posts = new WP_Query([
		'posts_per_page' => -1,
		'post_type' => $post_type,
		'suppress_filters' => false,
]);
switch ($post_type) {
	case 'event':
		$more = 'לחצו לעוד אירועים';
		break;
	case 'department':
		$more = 'לחצו לעוד מחלקות';
		break;
	default:
		$more = 'לחצו לעוד מאמרים';
		break;
}
?>
<article class="page-body">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-12">
				<h1 class="block-title">
					<?php the_title(); ?>
				</h1>
				<h2 class="block-subtitle">
					<?= $fields['blog_subtitle']; ?>
				</h2>
			</div>
			<div class="col-12">
				<div class="base-output text-center mb-4">
					<?php the_content(); ?>
				</div>
			</div>
		</div>
		<?php if ($posts->have_posts()) : ?>
			<div class="row align-items-stretch put-here-posts justify-content-center">
				<?php foreach ($posts->posts as $post) {
					get_template_part('views/partials/card', 'post',
						[
							'post' => $post,
						]);
				} ?>
			</div>
		<?php endif;
		if ($published_posts->have_posts() && (($num = count($published_posts->posts)) > 8)) : ?>
			<div class="row justify-content-center mt-4">
				<div class="col-auto">
					<div class="more-link base-link load-more-posts" data-type="<?= $post_type; ?>" data-count="<?= $num; ?>">
						<?= esc_html__($more, 'leos'); ?>
					</div>
				</div>
			</div>
		<?php endif; ?>
	</div>
</article>
<?php get_template_part('views/partials/repeat', 'form');
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
		'content' => $fields['single_slider_seo'],
		'img' => $fields['slider_img'],
	]);
}
get_footer(); ?>

