<?php
if(CATALOG){
    function product_post_type() {

        $labels = array(
            'name'                => 'מוצרים',
            'singular_name'       => 'מוצרים',
            'menu_name'           => 'מוצרים',
            'parent_item_colon'   => 'פריט אב:',
            'all_items'           => 'כל המוצרים',
            'view_item'           => 'הצג מוצר',
            'add_new_item'        => 'הוסף מוצר חדש',
            'add_new'             => 'הוסף חדש',
            'edit_item'           => 'ערוך מוצר',
            'update_item'         => 'עדכון מוצר',
            'search_items'        => 'חפש מוצר',
            'not_found'           => 'לא נמצא',
            'not_found_in_trash'  => 'לא מצא באשפה',
        );
        $rewrite = array(
            'slug'                => 'product',
            'with_front'          => true,
            'pages'               => true,
            'feeds'               => true,
        );
        $args = array(
            'label'               => 'product',
            'description'         => 'מוצרים',
            'labels'              => $labels,
            'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail'),
            'taxonomies'          => array( 'product_cat' ),
            'hierarchical'        => false,
            'public'              => true,
            'show_ui'             => true,
            'show_in_menu'        => true,
            'show_in_nav_menus'   => true,
            'show_in_admin_bar'   => true,
            'menu_position'       => 5,
            'menu_icon'           => 'dashicons-products',
            'can_export'          => true,
            'has_archive'         => true,
            'exclude_from_search' => false,
            'publicly_queryable'  => true,
            'rewrite'             => $rewrite,
            'capability_type'     => 'post',
        );
        register_post_type( 'product', $args );

    }

    add_action( 'init', 'product_post_type', 0 );

    function product_taxonomy() {

        $labels = array(
            'name'                       => 'קטגוריות מוצרים',
            'singular_name'              => 'קטגוריות מוצרים',
            'menu_name'                  => 'קטגוריות מוצרים',
            'all_items'                  => 'כל הקטגוריות',
            'parent_item'                => 'קטגורית הורה',
            'parent_item_colon'          => 'קטגורית הורה:',
            'new_item_name'              => 'שם קטגוריה חדשה',
            'add_new_item'               => 'להוסיף קטגוריה חדשה',
            'edit_item'                  => 'ערוך קטגוריה',
            'update_item'                => 'עדכן קטגוריה',
            'separate_items_with_commas' => 'קטגוריות נפרדות עם פסיק',
            'search_items'               => 'חיפוש קטגוריות',
            'add_or_remove_items'        => 'להוסיף או להסיר קטגוריות',
            'choose_from_most_used'      => 'בחר מהקטגוריות הנפוצות ביותר',
            'not_found'                  => 'לא נמצא',
        );
        $rewrite = array(
            'slug'                       => 'product_cat',
            'with_front'                 => true,
            'hierarchical'               => false,
        );
        $args = array(
            'labels'                     => $labels,
            'hierarchical'               => true,
            'public'                     => true,
            'show_ui'                    => true,
            'show_admin_column'          => true,
            'show_in_nav_menus'          => true,
            'show_tagcloud'              => true,
            'rewrite'                    => $rewrite,
        );
        register_taxonomy( 'product_cat', array( 'product' ), $args );

    }

    add_action( 'init', 'product_taxonomy', 0 );
}

if(PROJECTS){
    function project_post_type() {

        $labels = array(
            'name'                => 'פרויקטים',
            'singular_name'       => 'פרויקטים',
            'menu_name'           => 'פרויקטים',
            'parent_item_colon'   => 'פריט אב:',
            'all_items'           => 'כל הפרויקטים',
            'view_item'           => 'הצג פרויקט',
            'add_new_item'        => 'הוסף פרויקט',
            'add_new'             => 'הוסף פרויקט חדש',
            'edit_item'           => 'ערוך פרויקט',
            'update_item'         => 'עדכון פרויקט',
            'search_items'        => 'חפש פריטים',
            'not_found'           => 'לא נמצא',
            'not_found_in_trash'  => 'לא מצא באשפה',
        );
        $rewrite = array(
            'slug'                => 'project',
            'with_front'          => true,
            'pages'               => true,
            'feeds'               => true,
        );
        $args = array(
            'label'               => 'project',
            'description'         => 'פרויקטים',
            'labels'              => $labels,
            'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail'),
            'taxonomies'          => array( 'project_cat' ),
            'hierarchical'        => false,
            'public'              => true,
            'show_ui'             => true,
            'show_in_menu'        => true,
            'show_in_nav_menus'   => true,
            'show_in_admin_bar'   => true,
            'menu_position'       => 5,
            'menu_icon'           => 'dashicons-portfolio',
            'can_export'          => true,
            'has_archive'         => true,
            'exclude_from_search' => false,
            'publicly_queryable'  => true,
            'rewrite'             => $rewrite,
            'capability_type'     => 'post',
        );
        register_post_type( 'project', $args );

    }

    add_action( 'init', 'project_post_type', 0 );

    function project_taxonomy() {

        $labels = array(
            'name'                       => 'קטגוריות פרויקטים',
            'singular_name'              => 'קטגוריות פרויקטים',
            'menu_name'                  => 'קטגוריות פרויקטים',
            'all_items'                  => 'כל הקטגוריות',
            'parent_item'                => 'קטגורית הורה',
            'parent_item_colon'          => 'קטגורית הורה:',
            'new_item_name'              => 'שם קטגוריה חדשה',
            'add_new_item'               => 'להוסיף קטגוריה חדשה',
            'edit_item'                  => 'ערוך קטגוריה',
            'update_item'                => 'עדכן קטגוריה',
            'separate_items_with_commas' => 'קטגוריות נפרדות עם פסיק',
            'search_items'               => 'חיפוש קטגוריות',
            'add_or_remove_items'        => 'להוסיף או להסיר קטגוריות',
            'choose_from_most_used'      => 'בחר מהקטגוריות הנפוצות ביותר',
            'not_found'                  => 'לא נמצא',
        );
        $rewrite = array(
            'slug'                       => 'project_cat',
            'with_front'                 => true,
            'hierarchical'               => false,
        );
        $args = array(
            'labels'                     => $labels,
            'hierarchical'               => true,
            'public'                     => true,
            'show_ui'                    => true,
            'show_admin_column'          => true,
            'show_in_nav_menus'          => true,
            'show_tagcloud'              => true,
            'rewrite'                    => $rewrite,
        );
        register_taxonomy( 'project_cat', array( 'project' ), $args );

    }

    add_action( 'init', 'project_taxonomy', 0 );
}
if (DEPARTMENTS) {
	function department_post_type()
	{

		$labels = array(
			'name' => 'מחלקות',
			'singular_name' => 'מחלקה',
			'menu_name' => 'מחלקות',
			'parent_item_colon' => 'פריט אב:',
			'all_items' => 'כל המחלקות',
			'view_item' => 'הצג מחלקה',
			'add_new_item' => 'הוסף מחלקה חדשה',
			'add_new' => 'הוסף חדשה',
			'edit_item' => 'ערוך מחלקה',
			'update_item' => 'עדכון מחלקה',
			'search_items' => 'חפש מחלקה',
			'not_found' => 'לא נמצא',
			'not_found_in_trash' => 'לא מצא באשפה',
		);
		$rewrite = array(
			'slug' => 'department',
			'with_front' => true,
			'pages' => true,
			'feeds' => true,
		);
		$args = array(
			'label' => 'department',
			'description' => 'מחלקות',
			'labels' => $labels,
			'supports' => array('title', 'editor', 'excerpt', 'author', 'thumbnail'),
			'taxonomies' => array('department_cat'),
			'hierarchical' => false,
			'public' => true,
			'show_ui' => true,
			'show_in_menu' => true,
			'show_in_nav_menus' => true,
			'show_in_admin_bar' => true,
			'menu_position' => 5,
			'menu_icon' => 'dashicons-admin-multisite',
			'can_export' => true,
			'has_archive' => true,
			'exclude_from_search' => false,
			'publicly_queryable' => true,
			'rewrite' => $rewrite,
			'capability_type' => 'post',
		);
		register_post_type('department', $args);

	}

	add_action('init', 'department_post_type', 0);

	function department_taxonomy()
	{

		$labels = array(
			'name' => 'קטגוריות מחלקות',
			'singular_name' => 'קטגוריות מחלקות',
			'menu_name' => 'קטגוריות מחלקות',
			'all_items' => 'כל המחלקות',
			'parent_item' => 'קטגורית הורה',
			'parent_item_colon' => 'קטגורית הורה:',
			'new_item_name' => 'שם קטגוריה חדשה',
			'add_new_item' => 'להוסיף קטגוריה חדשה',
			'edit_item' => 'ערוך קטגוריה',
			'update_item' => 'עדכן קטגוריה',
			'separate_items_with_commas' => 'קטגוריות נפרדות עם פסיק',
			'search_items' => 'חיפוש קטגוריות',
			'add_or_remove_items' => 'להוסיף או להסיר קטגוריות',
			'choose_from_most_used' => 'בחר מהקטגוריות הנפוצות ביותר',
			'not_found' => 'לא נמצא',
		);
		$rewrite = array(
			'slug' => 'department_cat',
			'with_front' => true,
			'hierarchical' => false,
		);
		$args = array(
			'labels' => $labels,
			'hierarchical' => true,
			'public' => true,
			'show_ui' => true,
			'show_admin_column' => true,
			'show_in_nav_menus' => true,
			'show_tagcloud' => true,
			'rewrite' => $rewrite,
		);
		register_taxonomy('department_cat', array('department'), $args);

	}

	add_action('init', 'department_taxonomy', 0);
}
if (EVENTS) {
	function event_post_type()
	{

		$labels = array(
			'name' => 'אירועים',
			'singular_name' => 'אירוע',
			'menu_name' => 'אירועים',
			'parent_item_colon' => 'פריט אב:',
			'all_items' => 'כל האירועים',
			'view_item' => 'הצג אירוע',
			'add_new_item' => 'הוסף אירוע חדש',
			'add_new' => 'הוסף חדש',
			'edit_item' => 'ערוך אירוע',
			'update_item' => 'עדכון אירוע',
			'search_items' => 'חפש אירוע',
			'not_found' => 'לא נמצא',
			'not_found_in_trash' => 'לא מצא באשפה',
		);
		$rewrite = array(
			'slug' => 'event',
			'with_front' => true,
			'pages' => true,
			'feeds' => true,
		);
		$args = array(
			'label' => 'event',
			'description' => 'אירועים',
			'labels' => $labels,
			'supports' => array('title', 'editor', 'excerpt', 'author', 'thumbnail'),
			'taxonomies' => array('event_cat'),
			'hierarchical' => false,
			'public' => true,
			'show_ui' => true,
			'show_in_menu' => true,
			'show_in_nav_menus' => true,
			'show_in_admin_bar' => true,
			'menu_position' => 5,
			'menu_icon' => 'dashicons-admin-multisite',
			'can_export' => true,
			'has_archive' => true,
			'exclude_from_search' => false,
			'publicly_queryable' => true,
			'rewrite' => $rewrite,
			'capability_type' => 'post',
		);
		register_post_type('event', $args);

	}

	add_action('init', 'event_post_type', 0);

	function event_taxonomy()
	{

		$labels = array(
			'name' => 'קטגוריות אירועי',
			'singular_name' => 'קטגוריות אירועי',
			'menu_name' => 'קטגוריות אירועי',
			'all_items' => 'כל האירועים',
			'parent_item' => 'קטגורית הורה',
			'parent_item_colon' => 'קטגורית הורה:',
			'new_item_name' => 'שם קטגוריה חדשה',
			'add_new_item' => 'להוסיף קטגוריה חדשה',
			'edit_item' => 'ערוך קטגוריה',
			'update_item' => 'עדכן קטגוריה',
			'separate_items_with_commas' => 'קטגוריות נפרדות עם פסיק',
			'search_items' => 'חיפוש קטגוריות',
			'add_or_remove_items' => 'להוסיף או להסיר קטגוריות',
			'choose_from_most_used' => 'בחר מהקטגוריות הנפוצות ביותר',
			'not_found' => 'לא נמצא',
		);
		$rewrite = array(
			'slug' => 'event_cat',
			'with_front' => true,
			'hierarchical' => false,
		);
		$args = array(
			'labels' => $labels,
			'hierarchical' => true,
			'public' => true,
			'show_ui' => true,
			'show_admin_column' => true,
			'show_in_nav_menus' => true,
			'show_tagcloud' => true,
			'rewrite' => $rewrite,
		);
		register_taxonomy('event_cat', array('event'), $args);

	}

	add_action('init', 'event_taxonomy', 0);
}
