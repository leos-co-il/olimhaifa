<?php
get_header();
$query = get_queried_object();
$post_type = 'event';
$posts = new WP_Query([
	'posts_per_page' => 8,
	'post_type' => $post_type,
	'suppress_filters' => false,
	'tax_query' => array(
		array(
			'taxonomy' => 'event_cat',
			'field' => 'term_id',
			'terms' => $query->term_id,
		)
	)
]);
$published_posts = new WP_Query([
	'posts_per_page' => -1,
	'post_type' => $post_type,
	'suppress_filters' => false,
	'tax_query' => array(
		array(
			'taxonomy' => 'event_cat',
			'field' => 'term_id',
			'terms' => $query->term_id,
		)
	)
]);
$more = 'לחצו לעוד אירועים';
?>
<article class="page-body">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-12">
				<h1 class="block-title">
					<?= $query->name; ?>
				</h1>
			</div>
			<div class="col-12">
				<div class="base-output text-center mb-4">
					<?= category_description(); ?>
				</div>
			</div>
		</div>
		<?php if ($posts->have_posts()) : ?>
			<div class="row align-items-stretch put-here-posts justify-content-center">
				<?php foreach ($posts->posts as $post) {
					get_template_part('views/partials/card', 'post',
						[
							'post' => $post,
						]);
				} ?>
			</div>
		<?php endif;
		if ($published_posts->have_posts() && (($num = count($published_posts->posts)) > 8)) : ?>
			<div class="row justify-content-center mt-4">
				<div class="col-auto">
					<div class="more-link base-link load-more-posts" data-type="<?= $post_type; ?>" data-count="<?= $num; ?>"
						 data-term="<?= $query->term_id; ?>" data-term_name="event_cat">
						<?= esc_html__($more, 'leos'); ?>
					</div>
				</div>
			</div>
		<?php endif; ?>
	</div>
</article>
<?php get_template_part('views/partials/repeat', 'form');
if ($slider = get_field('single_slider_seo', $query)) {
	get_template_part('views/partials/content', 'slider', [
		'content' => $slider,
		'img' => get_field('slider_img', $query),
	]);
}
get_footer(); ?>

