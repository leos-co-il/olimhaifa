<?php

$tel = opt('tel');
$mail = opt('mail');
$address = opt('address');
$fax = opt('fax');

$locations = get_nav_menu_locations();
$menu_obj = wp_get_nav_menu_object($locations['footer-links-menu']);
$links_menu_title = get_field('menu_title', $menu_obj);
$menu_obj_main = wp_get_nav_menu_object($locations['footer-menu']);
$foo_menu_title = get_field('menu_title', $menu_obj_main);
$menu_obj_services = wp_get_nav_menu_object($locations['footer-services-menu']);
$foo_services_title = get_field('menu_title', $menu_obj_services);
?>
<footer>
	<div class="container">
		<div class="row">
			<div class="col-12">
				<a id="go-top">
					<img src="<?= ICONS ?>to-top.png">
					<h4 class="top-text">
						<?= esc_html__('חזרה למעלה', 'leos'); ?>
					</h4>
				</a>
			</div>
		</div>
	</div>
	<div class="footer-main">
		<div class="container">
			<div class="row align-items-end">
				<div class="col-lg-7 footer-form">
					<?php if ($f_title = opt('foo_form_title')) : ?>
						<h2 class="normal-block-title"><?= $f_title; ?></h2>
					<?php endif;
					if ($f_subtitle = opt('foo_form_subtitle')) : ?>
						<h3 class="normal-block-subtitle"><?= $f_subtitle; ?></h3>
					<?php endif;
					getForm('7'); ?>
				</div>
				<div class="col-1 col-hidden-foo"></div>
				<div class="col-xl-auto col-lg-5 col-12">
					<h3 class="foo-title foo-title-contact">
						<?= esc_html__('בואו נשאר בקשר', 'leos'); ?>
					</h3>
					<div class="menu-border-top">
						<ul class="contact-list d-flex flex-column">
							<?php if ($address) : ?>
								<li>
									<a href="https://waze.com/ul?q=<?= $address; ?>" class="contact-info-footer" target="_blank">
										<span>
											<img src="<?= ICONS ?>foo-geo.png" alt="geo">
										</span>
										<?= $address; ?>
									</a>
								</li>
							<?php endif;
							if ($mail) : ?>
								<li>
									<a href="mailto:<?= $mail; ?>" class="contact-info-footer">
										<span>
											<img src="<?= ICONS ?>foo-mail.png" alt="email">
										</span>
										<?= $mail; ?>
									</a>
								</li>
							<?php endif;
							if ($tel) : ?>
								<li>
									<a href="tel:<?= $tel; ?>" class="contact-info-footer">
										<span>
											<img src="<?= ICONS ?>foo-tel.png" alt="phone">
										</span>
										<?= $tel; ?>
									</a>
								</li>
							<?php endif; ?>
						</ul>
						<div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="container footer-container-menu">
			<div class="row justify-content-sm-between justify-content-center align-items-stretch foo-after-title">
				<div class="col-lg-auto col-sm-6 col-12 foo-menu">
					<h3 class="foo-title">
						<?= $foo_menu_title ? $foo_menu_title : esc_html__('ניווט מהיר', 'leos'); ?>
					</h3>
					<div class="menu-border-top">
						<?php getMenu('footer-menu', '1'); ?>
					</div>
				</div>
				<div class="col-lg-auto col-sm-6 col-12 foo-menu services-footer-menu">
					<h3 class="foo-title">
						<?= $foo_services_title ? $foo_services_title : esc_html__('השירותים שלנו', 'leos'); ?>
					</h3>
					<div class="menu-border-top">
						<?php getMenu('footer-services-menu', '1', 'hop-hey two-columns'); ?>
					</div>
				</div>
				<div class="col-lg-auto col-12 foo-menu foo-links-menu">
					<div>
						<h3 class="foo-title">
							<?= $links_menu_title ? $links_menu_title : esc_html__('חם מהבלוג', 'leos'); ?>
						</h3>
						<div class="menu-border-top">
							<?php getMenu('footer-links-menu', '1', 'hop-hey two-columns'); ?>
						</div>
					</div>
					<?php if ($facebook = opt('facebook')) : ?>
						<a href="<?= $facebook; ?>" class="foo-facebook">
							<span><?= esc_html__('עקבו אחרינו בפייסבוק', 'leos'); ?></span>
							<img src="<?= ICONS ?>facebook-like.png" alt="facebook">
						</a>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
	<?php if ($logos = opt('foo_logos')) : ?>
		<div class="foo-logo-line">
			<div class="container">
				<div class="row justify-content-center align-items-center">
					<?php foreach ($logos as $logo) : ?>
						<div class="col-md col-sm-4 col-6">
							<div class="foo-logo-item">
								<img src="<?= $logo['url']; ?>" alt="<?= $logo['alt']; ?>">
							</div>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
	<?php endif; ?>
	<div id="leos">
		<a href="http://www.leos.co.il/" title="לאוס מדיה ואינטראקטיב">
			<img src="<?= IMG . 'leos_logo.png' ?>"
				 alt="" title="קידום אתרים עם לאוס מדיה ואינטראקטיב | חברה לקידום אתרים ובניית אתרים" />
			<span></span>
		</a>
	</div>
</footer>


<?php wp_footer(); ?>

<?php

if(ENV === 'dev'):
	require_once THEMEPATH . "/inc/debug.php"
	?>
	<script>

		function _fetchHeader($_el){
			let res = {
				'count' : 0,
				'content' : ''
			} ;
			$($_el).each(function () {
				res.count++;
				res.content += ' [' + $(this).text() + '] ';
			});
			return 'Count: ' + res.count + '. Text: ' + res.content;
		}

		function _fetchMeta($_meta){
			return $('meta[name='+$_meta+']').attr("content");
		}




		phpdebugbar.addDataSet({
			"SEO Local": {
				'H1' : _fetchHeader('h1'),
				'H2' : _fetchHeader('h2'),
				'H3' : _fetchHeader('h3'),
				'Meta Title' : _fetchMeta('title'),
				'Meta Description' : _fetchMeta('description'),
				'Meta Keywords' : _fetchMeta('keywords'),
			}
		});
	</script>

<?php endif; ?>

</body>
</html>
