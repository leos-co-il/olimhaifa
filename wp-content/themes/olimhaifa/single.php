<?php
the_post();
get_header();
$fields = get_fields();
?>

<article class="post-page-body">
	<section class="post-top">
		<?php if ($fields['post_top_img']) : ?>
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-xl-4 col-lg-5 col-sm-6 col-8">
						<img src="<?= $fields['post_top_img']['url']; ?>" alt="<?= $fields['post_top_img']['alt']; ?>">
					</div>
				</div>
			</div>
		<?php endif; ?>
	</section>
	<?php get_template_part('views/partials/content', 'slider_departments'); ?>
	<div class="post-article">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-auto">
					<div class="title-wrapper-post">
						<h1 class="block-title text-center">
							<?php the_title(); ?>
						</h1>
						<h2 class="block-subtitle text-center">
							<?= $fields['post_subtitle']; ?>
						</h2>
					</div>
				</div>
			</div>
			<div class="row justify-content-center align-items-start">
				<?php if ($fields['post_sidebar']) : ?>
					<div class="col-xl-3 col-lg-4 col-md-6 col-12 sidebar-col">
						<div id="accordion-sidebar">
							<?php foreach ($fields['post_sidebar'] as $num => $item) : ?>
								<div class="card wow fadeInUp" data-wow-delay="0.<?= $num + $i = 1; ?>s" <?php $i++; ?>>
									<div class="post-side-wrap" id="sidebar-heading_<?= $num; ?>">
										<button class="post-sidebar-item" data-toggle="collapse"
												data-target="#sideChild<?= $num; ?>"
												aria-expanded="false" aria-controls="collapseOne">
											<?= $item['title']; ?>
										</button>
										<?php if ($item['links']) : ?>
											<div id="sideChild<?= $num; ?>" class="collapse"
												 aria-labelledby="sidebar-heading_<?= $num; ?>" data-parent="#accordion-sidebar">
												<div class="links-sidebar">
													<?php foreach ($item['links'] as $link) : if ($link['link_url']) : ?>
														<a href="<?= $link['link_url']['url']; ?>" class="side-link">
															<?= isset($link['link_url']['title']) ? $link['link_url']['title'] : ''; ?>
														</a>
													<?php endif; endforeach; ?>
												</div>
											</div>
										<?php endif; ?>
									</div>
								</div>
							<?php endforeach; ?>
						</div>
					</div>
				<?php endif; ?>
				<div class="<?= $fields['gallery'] || has_post_thumbnail() || $fields['post_sidebar'] ? 'col-xl-6 col-lg col-12' : 'col'; ?> col-post-content">
					<div class="base-output">
						<?php the_content(); ?>
					</div>
				</div>
				<?php if ($fields['gallery'] || has_post_thumbnail()) : ?>
					<div class="col-xl-3 col-12 post-gallery-col">
						<div class="single-gallery-slider arrows-slider">
							<div class="post-gallery-slider">
								<?php if (has_post_thumbnail()) : ?>
									<div class="p-1">
										<a href="<?= postThumb(); ?>" data-lightbox="post-gallery" class="single-gallery-item"
										   style="background-image: url('<?= postThumb(); ?>')"></a>
									</div>
								<?php endif;
								foreach ($fields['gallery'] as $img) : ?>
									<div class="p-1">
										<a href="<?= $img['url']; ?>" data-lightbox="post-gallery" class="single-gallery-item"
										style="background-image: url('<?= $img['url']; ?>')"></a>
									</div>
								<?php endforeach; ?>
							</div>
						</div>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</article>
<?php if ($fields['post_slider']) :
	$link = ($fields['post_slider_link']) ? $fields['post_slider_link'] : '';
	$link_title = (isset($args['link_title']) && $args['link_title']) ? $args['link_title'] : 'לכל המאמרים';
	?>
	<section class="home-posts-block arrows-slider pt-5 post-about-slider">
		<div class="container">
			<div class="row">
				<div class="col-12 mb-3">
					<?php if ($fields['post_slider_title']) : ?>
						<h2 class="block-title text-center">
							<?= $fields['post_slider_title']; ?>
						</h2>
					<?php endif; ?>
				</div>
			</div>
			<div class="row justify-content-center align-items-stretch">
				<div class="col-12">
					<div class="post-slider" dir="rtl">
						<?php foreach ($fields['post_slider'] as $post) : ?>
							<div>
								<?php get_template_part('views/partials/card', 'post', [
									'post' => $post,
								]); ?>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
			<?php if ($link) : ?>
				<div class="row justify-content-center mt-4">
					<div class="col-auto">
						<a href="<?= $link['url'];?>" class="base-link">
							<?= (isset($link['title']) && $link['title']) ? $link['title'] : $link_title; ?>
						</a>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</section>
<?php endif; ?>
<section class="repeat-form-post">
	<div class="form-post-overlay">
		<div class="container">
			<div class="row justify-content-center form-wrapper">
				<div class="col-12">
					<?php if ($title = (isset($fields['post_form_title']) && $fields['post_form_title']) ? $fields['post_form_title'] : opt('post_form_title')) : ?>
						<h2 class="normal-block-title text-center"><?= $title; ?></h2>
					<?php endif;
					if ($subtitle = (isset($fields['post_form_subtitle']) && $fields['post_form_subtitle']) ? $fields['post_form_subtitle'] : opt('post_form_subtitle')) : ?>
						<h3 class="normal-block-subtitle text-center"><?= $subtitle; ?></h3>
					<?php endif; ?>
				</div>
				<div class="col-xl-5 col-lg-8 col-md-10 col-12">
					<?php getForm('11'); ?>
				</div>
			</div>
		</div>
	</div>
</section>
<?php
if ($fields['faq_item']) :
	get_template_part('views/partials/content', 'faq',
			[
					'text' => $fields['faq_text'],
					'faq' => $fields['faq_item'],
			]);
endif;
get_footer(); ?>
