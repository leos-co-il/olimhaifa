<!DOCTYPE HTML>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <link rel="icon" href="<?= IMG ?>favicon.ico" type="image/x-icon">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    <?php wp_head(); ?>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body <?php body_class(ENV); ?>>

<?php if(ENV === 'dev'): ?>
    <script>
        const timerStart = Date.now();
    </script>
<div class="debug bg-danger border">
    <p class="width">
        <span>Width:</span>
        <span class="val"></span>
    </p>
    <p class="height">
        <span>Height:</span>
        <span class="val"></span>
    </p>
    <p class="media-query">
        <span>Media Query:</span>
        <span class="val"></span>
    </p>
    <p class="zoom">
        <span>Zoom:</span>
        <span class="val"></span>
    </p>
    <p class="dom-ready">
        <span>DOM Ready:</span>
        <span class="val"></span>
    </p>
    <p class="load-time">
        <span>Loading Time:</span>
        <span class="val"></span>
    </p>
</div>
<?php endif; ?>


<header>
	<div class="header-top">
		<div class="drop-menu">
			<nav class="drop-nav">
				<?php getMenu('dropdown-menu', '1', '', ''); ?>
			</nav>
		</div>
		<div class="container">
			<div class="row align-items-center justify-content-between">
				<?php if ($logo = opt('logo')) : ?>
					<div class="col-auto">
						<a href="<?= home_url(); ?>" class="logo">
							<img src="<?= $logo['url'] ?>" alt="logo">
						</a>
					</div>
				<?php endif; ?>
				<div class="col d-flex align-items-center position-relative">
					<button class="hamburger hamburger--spin menu-trigger" type="button">
					<span class="hamburger-box">
						<span class="hamburger-inner"></span>
					</span>
					</button>
					<nav id="MainNav" class="h-100">
						<?php getMenu('header-menu', '1', '', 'main_menu h-100'); ?>
					</nav>
					<?php get_search_form(); ?>
				</div>
				<div class="col-auto d-flex justify-content-end align-items-center">
					<?php do_action('wpml_add_language_selector');
					if ($haifa = opt('logo_haifa')) : ?>
						<div class="haifa-logo">
							<img src="<?= $haifa['url']; ?>" alt="haifa">
						</div>
					<?php endif;
					if ($logo_country = opt('logo_country')) : ?>
						<div class="country-logo">
							<img src="<?= $logo_country['url']; ?>" alt="israel">
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
	<div class="header-bottom">
		<div class="container">
			<div class="row justify-content-end">
				<div class="col-xl-9 col d-flex justify-content-between align-items-center">
					<img src="<?= ICONS ?>ole.png" alt="ole" class="ole-icon">
					<p class="header-bottom-title">
						<?= esc_html__('כבר הצטרפתם למועדון הלקוחות שלנו?', 'leos'); ?>
					</p>
					<?php if ($header_link = opt('ban_link')) : ?>
						<div class="position-relative">
							<a href="<?= $header_link['url']; ?>" class="header-link">
							<span class="head-link-text">
								<?= isset($header_link['title']) && $header_link['title'] ? $header_link['title'] : esc_html__('הכנסו ותתעדכנו!', 'leos'); ?>
							</span>
							</a>
							<span class="header-link-rotated"></span>
						</div>
					<?php endif; ?>
					<img src="<?= ICONS ?>ole.png" alt="ole" class="ole-icon">
				</div>
				<div class="col">
					<?php if ($rus = opt('ban_ru_link')) : ?>
						<a class="link-for-rus" href="<?= $rus['url']; ?>">
							Русскоговорящие?
							<span class="font-weight-bold">Нажмите сюда</span>
						</a>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</header>

<section class="pop-form">
	<div class="container">
		<div class="row justify-content-end">
			<div class="col-xl-8 col-lg-10 col-12 d-flex justify-content-center">
				<div class="float-form d-flex flex-column align-items-center">
					<div class="pop-form-back">
						<span class="close-form">
							x
						</span>
						<?php if ($f_title = opt('pop_form_title')) : ?>
							<h2 class="normal-block-title"><?= $f_title; ?></h2>
						<?php endif;
						if ($f_subtitle = opt('pop_form_subtitle')) : ?>
							<h3 class="normal-block-subtitle"><?= $f_subtitle; ?></h3>
						<?php endif;
						getForm('6'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<?php
$chat_link = opt('fix_link');
$contact_page = getPageByTemplate('views/contact.php');
?>
<div class="chat-trigger <?= is_home() ? 'front-back' : ''; ?>" href="<?= $chat_link ? $chat_link['url'] : get_permalink($contact_page); ?>">
	<span class="chat-trigger-text">
		<?= esc_html__('מחלקת פניות לציבור', 'leos'); ?>
	</span>
</div>
<div class="triggers-col">
	<div class="trigger-item pop-trigger <?= is_home() ? 'front-back' : ''; ?>">
		<span class="pop-trigger-text">
			<?= esc_html__('צור קשר', 'leos'); ?>
		</span>
	</div>
	<?php if ($telegram = opt('telegram')) : ?>
		<div class="trigger-item mb-1">
			<a href="<?= $telegram; ?>" class="social-link-item" target="_blank">
				<img src="<?= ICONS ?>telegram.png" alt="telegram">
			</a>
		</div>
	<?php endif;
	if ($instagram = opt('instagram')) : ?>
		<div class="trigger-item mb-1">
			<a href="<?= $instagram; ?>" class="social-link-item" target="_blank">
				<img src="<?= ICONS ?>instagram.png" alt="instagram">
			</a>
		</div>
	<?php endif;
	if ($facebook = opt('facebook')) : ?>
		<div class="trigger-item mb-1">
			<a href="<?= $facebook; ?>" class="social-link-item" target="_blank">
				<img src="<?= ICONS ?>facebook.png" alt="facebook">
			</a>
		</div>
	<?php endif; ?>
</div>
